<?php

declare(strict_types=1);

namespace Jivix\Laravel\ReCaptcha;

use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Jivix\Laravel\ReCaptcha\Services\ReCaptchaService;
use Jivix\ReCaptcha\Client;

class ReCaptchaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application events.
     *
     * @return void
     * @throws BindingResolutionException
     */
    public function boot(): void
    {
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'recaptcha');

        $this->extendValidator();
    }

    /**
     * @throws BindingResolutionException
     */
    protected function extendValidator(): void
    {
        /** @var Translator $translator */
        $translator = $this->app->make(Translator::class);

        /** @var Validator $validator */
        $validator = $this->app->make('validator');

        $validator->extend('recaptcha', function ($attribute, $value) {
            $container = Container::getInstance();

            /** @var ReCaptchaService $reCaptchaService */
            $reCaptchaService = $container->make(ReCaptchaService::class);

            return $reCaptchaService->verify($value);
        }, $translator->get('recaptcha::validation.verified'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/services.php', 'services');

        $this->bindReCaptchaClient();
    }

    /**
     * Bind the reCAPTCHA client.
     * @return void
     */
    protected function bindReCaptchaClient(): void
    {
        $this->app->bind(Client::class, function () {
            $secretKey = $this->app->make('config')->get('services.recaptcha.secret_key');

            return new Client($secretKey);
        });
    }
}