<?php

declare(strict_types=1);

namespace Jivix\Laravel\ReCaptcha\Rules;

use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Contracts\Validation\Rule;
use Jivix\Laravel\ReCaptcha\Services\ReCaptchaService;

class ReCaptcha implements Rule
{
    /**
     * @var Container
     */
    private Container $container;

    /**
     * ReCaptcha constructor.
     */
    public function __construct()
    {
        $this->container = Container::getInstance();
    }

    /**
     * @param string $attribute
     * @param mixed $value
     * @return bool
     * @throws BindingResolutionException
     */
    public function passes($attribute, $value): bool
    {
        /** @var ReCaptchaService $reCaptchaService */
        $reCaptchaService = $this->container
            ->make(ReCaptchaService::class);

        return $reCaptchaService->verify($value);
    }

    /**
     * @return string
     * @throws BindingResolutionException
     */
    public function message(): string
    {
        /** @var Translator $translator */
        $translator = $this->container->make(Translator::class);

        return $translator->get('recaptcha::validation.verified');
    }
}