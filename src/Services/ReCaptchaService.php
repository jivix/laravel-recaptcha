<?php

declare(strict_types=1);

namespace Jivix\Laravel\ReCaptcha\Services;

use Exception;
use Illuminate\Http\Request;
use Jivix\ReCaptcha\Client;

class ReCaptchaService
{
    /**
     * @var Client
     */
    private Client $client;

    /**
     * @var Request
     */
    private Request $request;

    /**
     * ReCaptchaService constructor.
     * @param Client $client
     * @param Request $request
     */
    public function __construct(Client $client, Request $request)
    {
        $this->client = $client;
        $this->request = $request;
    }

    /**
     * @param string $response
     * @return bool
     */
    public function verify(string $response): bool
    {
        try {
            $response = $this->client
                ->verify($response, $this->request->getClientIp() ?? null);

            return $response->isSuccess();
        } catch (Exception $e) {
            //
        }

        return false;
    }
}